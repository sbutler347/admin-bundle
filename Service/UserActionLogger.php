<?php

namespace DL\AdminBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\SecurityContext;
use DL\AdminBundle\Entity\ActionLogEntry;

class UserActionLogger
{
    protected $em;

    public function __construct(SecurityContext $securityContext, EntityManager $em)
    {
        $this->securityContext = $securityContext;
        $this->em = $em;
    }

    /**
     * Logs a component action
     *
     * @param string $component
     * @param string $data
     */
    public function log($component, $data)
    {
        $user = $this->securityContext->getToken()->getUser();

        $entry = new ActionLogEntry($user, $component, $data, $_SERVER['REMOTE_ADDR']);

        $this->em->persist($entry);
        $this->em->flush();
    }
}
