<?php

namespace DL\AdminBundle\Command;

use Symfony\Component\DependencyInjection\Exception\RuntimeException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use DL\AdminBundle\Entity\User;
use DL\AdminBundle\Entity\Privilege;

class CreateSuperUserCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('dl:admin:user:create')
                ->setDescription('Create an admin user and assign privileges as required')
                ->addOption("superuser", null, InputOption::VALUE_NONE, "Should we not ask and set every permission for this user?")
                ->addOption("password", null, InputOption::VALUE_REQUIRED, "For the provisioner, to avoid TTY issues with hidden password.");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em 		= $this->getContainer()->get('doctrine')->getManager();
        $factory 	= $this->getContainer()->get('security.encoder_factory');

        // grab all the privileges
        $allPrivileges = $em->getRepository('DLAdminBundle:Privilege')->findAll();

        // check we have some privileges
        if (0 === count($allPrivileges)) {
            $output->writeln("<error>The privileges have not yet been imported into the database. Please load/insert then try again.</error>");

            return false;
        }

        $dialog = $this->getHelperSet()->get('dialog');

        // validate the length of the users password
        $usernameValidator = function ($value) {
            if (trim($value) == '') {
                throw new RuntimeException('The username can not be empty');
            }

            if (strlen(trim($value)) < 4) {
                throw new RuntimeException('The username must be 6 chars or more');
            }

            return $value;
        };

        // get the username
        $username = $dialog->askAndValidate(
            $output,
            'Please enter a username: ',
            $usernameValidator,
            3
        );

        // get their email address
        $email = $dialog->ask(
            $output,
            'Please enter their email address: '
        );

        // validate the length of the users password
        $passwordValidator = function ($value) {
            if (trim($value) == '') {
                throw new RuntimeException('The password can not be empty');
            }

            if (strlen(trim($value)) < 6) {
                throw new RuntimeException('The password must be 6 chars or more');
            }

            return $value;
        };

        // get the users password
        if(!$password = $input->getOption("password")) {
            $password = $dialog->askHiddenResponseAndValidate(
                $output,
                'Please enter their password: ',
                $passwordValidator,
                3,
                false,
                "Invalid password"
            );
        }

        // get the username
        $fullName = $dialog->ask(
            $output,
            'Please enter their full name: '
        );

        // create the user entity
        $u	 		= new User();
        $u->setUsername($username);
        $u->setFullName($fullName);
        $u->setEmail($email);
        $u->setMobile('please update');

        // track privileges added
        $count = 0;

        // was the super user option used?
        $isSuperuser = $input->getOption("superuser");

        // check which privileges to add
        for ($i=0; $i<count($allPrivileges); $i++) {

            if(false === $isSuperuser) {
                $add = $dialog->ask(
                    $output,
                    sprintf('Add the following privilege: <info>%s</info> [no]: ', $allPrivileges[$i]->getName()),
                    'no',
                    ['yes', 'no']
                );
            }

            if ($isSuperuser || 'yes' == $add) {
                $u->addPrivilege($allPrivileges[$i]);
            }

            // inc the count
            $count++;
        }

        if (0 === $count) {
            $output->writeln("<error>No privileges were added. Exiting</error>");

            return;
        }

        $encoder 			= $factory->getEncoder($u);
        $encodedPassword 	= $encoder->encodePassword($password, $u->getSalt());

        $u->setPassword($encodedPassword);
        $em->persist($u);

        // flush it
        $em->flush();

        $output->writeln(sprintf('Added %s user with password %s', $username, $password));
    }
}
