<?php

namespace DL\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use DL\AdminBundle\Entity\Module;

class UpdateModulesCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('dl:modules:update')
            ->setDescription('Search for all @ModuleDefinition annotations and update as required');
    }

    /**
     * Searchs for ROLES that are not in the database and adds them if desired
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em 	        = $this->getContainer()->get('doctrine')->getManager();
        $repo 	        = $em->getRepository('DLAdminBundle:Module');

        $missingModules = array();

        // get the finder service
        $annotationsFinder = $this->getContainer()->get('dl.annotation_finder');

        // get all the module definition annotations
        $annotations = $annotationsFinder->getAnnotationsMatchingNameAndType("ModuleDefinition", "\\DL\\AdminBundle\\Annotation\\ModuleDefinition");

        // for each of the annotations
        for ($i=0; $i<count($annotations); $i++) {

            // we want to check that the bundle name and controller class does not already exist
            $entity = $repo->findOneBy(array(
                'bundleName'            => $annotations[$i]->getBundleName(),
                'adminControllerName'   => $annotations[$i]->getAdminControllerName()
            ));

            // if we cannot find this module
            if (!$entity) {

                // add it to the missing modules
                $missingModules[] = $annotations[$i];

            } else {
                $output->writeln(sprintf('<info>Matched entry with %s:%s</info>', $annotations[$i]->getBundleName(), $annotations[$i]->getAdminControllerName()));
            }
        }

        // if we have no missing modules then msg and return
        if (0 === count($missingModules)) {
            $output->writeln('<info>All defined modules exist in the database</info>');

            return;
        }

        // get the dialog helper
        $dialog = $this->getHelperSet()->get('dialog');

        // for each of the missing modules
        for ($i=0; $i<count($missingModules); $i++) {

            $m = $missingModules[$i];

            // check if we want to add it - default is yes
            if (!$dialog->askConfirmation($output, sprintf('<question>Do you wish to add the module: %s?</question> [yes]: ', $m->getName() ), true )) {
                continue;
            }

            // create and persist the module
            $module = new Module();
            $module->setName($m->getName());
            $module->setDescription($m->getDescription());
            $module->setBundleName($m->getBundleName());
            $module->setBundleClassPath($m->getBundleClassPath());
            $module->setEntityName($m->getEntityName());
            $module->setAdminControllerName($m->getAdminControllerName());
            $module->setAdminControllerRoute($m->getAdminControllerRoute());
            $module->setRequiredRole($m->getRequiredRole());
            $module->setMenuLevel($m->getMenuLevel());
            $module->setCreatedAt(new \DateTime());

            //
            $em->persist($module);
        }

        //
        $em->flush();
    }
}
