<?php

namespace DL\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use DL\AdminBundle\Entity\Privilege;

class PrivilegesUpdateCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('dl:admin:privileges:update')
            ->setDescription('Search for all @Secure annotations and adds the relevant privileges to the database');
    }

    /**
     * Searchs for ROLES that are not in the database and adds them if desired
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em 	= $this->getContainer()->get('doctrine')->getManager();
        $repo 	= $em->getRepository('DLAdminBundle:Privilege');

        $missingRoles = array();

        // get the finder service
        $annotationsFinder = $this->getContainer()->get('dl.annotation_finder');

        // get all the module definition annotations
        $annotations = $annotationsFinder->getAnnotationsMatchingNameAndType("AdminPrivilegeDefinition", "\\DL\\AdminBundle\\Annotation\\AdminPrivilegeDefinition");

        // if we have no missing roles then msg and return
        if (0 === count($annotations)) {
            $output->writeln('<error>No matching annotations found</error>');

            return;
        }

        // check for each roles existance in the db
        for ($i=0; $i<count($annotations); $i++) {

            $entity = $repo->findOneByRole($annotations[$i]->getRole());

            if (!$entity && $annotations[$i]->getRole() != 'ROLE_ADMIN') {
                $missingRoles[] = $annotations[$i];
            } else {
                $output->writeln(sprintf('<info>Matched entry with %s:%s</info>', $annotations[$i]->getName(), $annotations[$i]->getRole()));
            }
        }

        // if we have no missing roles then msg and return
        if (0 === count($missingRoles)) {
            $output->writeln('<info>All defined roles exist in the database</info>');

            return;
        }

        // get the dialog helper
        $dialog = $this->getHelperSet()->get('dialog');

        // for each of the missing privileges
        for ($i=0; $i<count($missingRoles); $i++) {

            $m = $missingRoles[$i];

            // check if we want to add it - default is yes
            if (!$dialog->askConfirmation($output, sprintf('<question>Do you wish to add the role: %s:%s?</question> [yes]: ', $m->getName(), $m->getRole()), true )) {
                continue;
            }

            // create and persist the privilege
            $privilege = new Privilege();
            $privilege->setRole($m->getRole());
            $privilege->setName($m->getName());
            $privilege->setDescription($m->getDescription());

            $em->persist($privilege);
        }

        //
        $em->flush();
    }
}
