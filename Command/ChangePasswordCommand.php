<?php

namespace DL\AdminBundle\Command;

use Symfony\Component\DependencyInjection\Exception\RuntimeException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use DL\AdminBundle\Entity\User;

class ChangePasswordCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('dl:admin:user:changepassword')
                ->setDescription('Allow the users password to be changed');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em 		= $this->getContainer()->get('doctrine')->getManager();
        $factory 	= $this->getContainer()->get('security.encoder_factory');
        
        $userRepository = $em->getRepository('DLAdminBundle:User');
        
        $dialog = $this->getHelperSet()->get('dialog');

        // validate the length of the users password
        $usernameValidator = function ($value) use ($userRepository) {
            try
            {
                $userRepository->loadUserByUsername($value);
            } catch (Exception $e) {
                throw new RuntimeException('User does not exist');
            }
            return $value;
        };

        // get the username
        $username = $dialog->askAndValidate(
            $output,
            'Please enter username: ',
            $usernameValidator,
            3
        );

        // validate the length of the users password
        $passwordValidator = function ($value) {
            if (trim($value) == '') {
                throw new RuntimeException('The password can not be empty');
            }

            if (strlen(trim($value)) < 6) {
                throw new RuntimeException('The password must be 6 chars or more');
            }

            return $value;
        };

        // get the users password
        $password = $dialog->askHiddenResponseAndValidate(
            $output,
            'Please enter a new password: ',
            $passwordValidator,
            3,
            false,
            "Invalid password"
        );

        // create the user entity
        $u	 		= $userRepository->loadUserByUsername($username);

        $encoder 			= $factory->getEncoder($u);
        $encodedPassword 	= $encoder->encodePassword($password, $u->getSalt());

        $u->setPassword($encodedPassword);
        $em->persist($u);

        // flush it
        $em->flush();

        $output->writeln(sprintf('User %s password updated %s', $username, $password));
    }
}
