<?php

namespace DL\AdminBundle\Command;

use Sensio\Bundle\GeneratorBundle\Command\Validators;
/**
 * Validator functions extension for Dotlabel CMS generator.
 */
class DLValidators extends Validators
{
    public static function validatePermission($permission)
    {
        $permission = strtoupper($permission);
        
        $permission = str_replace(" ", "_", $permission);

        return $permission;
    }
}
