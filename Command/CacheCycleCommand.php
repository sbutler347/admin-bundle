<?php

namespace DL\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\HttpKernel\Log\LoggerInterface;

class CacheCycleCommand extends ContainerAwareCommand
{
    /**
     * @TODO Extend to handle exceptions like http://symfony.com/doc/current/cookbook/console/logging.html
     */
    protected function configure()
    {
        $this->setName('dl:cache:cycle')
                ->setDescription('Runs cache:clear, assetic:dump and assets:install');
    }

    /**
     *
     */
    private function runCommand($command, $arguments, $input, $output)
    {
        $input      = new ArrayInput($arguments);
        $returnCode = $command->run($input, $output);
        
        // if the command failed 
        if (0 !== $returnCode) {
            $err = sprintf("An error occured running the command %s", $arguments['command']);
            $output->writeln(sprintf("<error>%s</error>"));
            $this->getContainer()->get('logger')->error($err);
        }

        return $returnCode;
    }

    /**
     * Runs cache:clear, assetic:dump and assets:install
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // clear the cache 
        $command    = $this->getApplication()->find('cache:clear');
        $arguments  = array(
            'command' => 'cache:clear',
            '--env'   => 'prod', 
        );
        
        // $this->runCommand($command, $arguments, $input, $output);
        
        // warm the cache 
        $command    = $this->getApplication()->find('cache:warmup');
        $arguments  = array(
            'command' => 'cache:warmup',
        );
        
        $this->runCommand($command, $arguments, $input, $output);

        // install the assets 
        $command    = $this->getApplication()->find('assets:install');
        $arguments  = array(
            'command'   => 'assets:install',
            '--symlink' => true,
        );

        $this->runCommand($command, $arguments, $input, $output);
        
        // dump the css and js assets 
        $command    = $this->getApplication()->find('assetic:dump');
        $arguments  = array(
            'command' => 'assetic:dump',
        );

        $this->runCommand($command, $arguments, $input, $output);
    }
}
