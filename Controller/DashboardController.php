<?php

namespace DL\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use DL\CoreBundle\Controller\Controller;

/**
 * @Route("/")
 */
class DashboardController extends Controller
{

    /**
     * @Route("", name="admin_home")
     * @Template()
     * @Secure("ROLE_ADMIN")
     */
    public function indexAction()
    {
        $data = array('pageTitle'=>'Admin Home', 'pageDescription'=>'Manage and control your website features and content');

        $em = $this->container->get('doctrine')->getManager();

        // the action log entries
        $actionLogQuery = $em->createQueryBuilder()
            ->from('DL\AdminBundle\Entity\ActionLogEntry', 'l')
            ->select('l, u')
            ->join('l.user', 'u')
            ->orderBy('l.createdAt', 'DESC')
            ->setMaxResults(10)
            ->getQuery();

        $data['actionLogEntries'] = $actionLogQuery->execute();
        $data['area'] = 'dashboard';

        return $data;
    }
}
