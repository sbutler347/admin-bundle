<?php

namespace DL\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use DL\CoreBundle\Controller\Controller;

/**
 * @Route("/")
 */
class AdminMenuController extends Controller
{

    /**
     * @Template()
     * @Secure("ROLE_ADMIN")
     */
    public function renderModulesAction($area = null)
    {
        // get the current user
        $user = $this->container->get('security.context')->getToken()->getUser();

        // get the admin modules
        $allModules = $this->container->get('doctrine')->getManager()->getRepository('DLAdminBundle:Module')->findAll();

        // holder for the permitted modules
        $topLevelModules = array();
        $subModules = array();

        for ($i=0; $i<count($allModules); $i++) {

            // @TODO ensure that this is not a page module (
            // for now we check the routeName and if empty we don't render the loop

            // check the user has the required role for the module
            if (true === $this->get('security.context')->isGranted($allModules[$i]->getRequiredRole())) {

                // if this is a top level module
                if (0 === $allModules[$i]->getMenuLevel()) {
                    $topLevelModules[] = $allModules[$i];
                } else {
                    $subModules[] = $allModules[$i];
                }

            }
        }

        return array(
            'topLevelModules' => $topLevelModules,
            'subModules' => $subModules,
            'area' => $area, 
        );
    }
}
