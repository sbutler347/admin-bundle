<?php

namespace DL\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use DL\CoreBundle\Controller\Controller;
use DL\AdminBundle\Entity\User;

/**
 * @Route("/auth")
 */
class PasswordResetController extends Controller
{
    /**
     * @Route("/reset", name="admin_password_reset")
     * @Template()
     */
    public function indexAction()
    {

        // show the form for their username/email
        $form = $this->createFormBuilder()
                    ->add('username', 'text')
                    ->getForm();

        // get the request
        $request = Request::createFromGlobals();

        // if data has been submitted then cool - show them the confirmation message
        if ($request->getMethod() == 'POST') {

                // see can we find a user with that username
                $username = $request->request->get('form')['username'];

                // now see can we find a user with that username
                if (!empty($username)) {

                    // try to get the user
                    try {

                        // try to get the user
                        $user = $this->getDoctrine()->getRepository('DLAdminBundle:User')->loadUserByUsername($username);

                        $length = 32;
                        $strong = true;

                        // generate a password reset request
                        $hash = base64_encode(openssl_random_pseudo_bytes($length, $strong));

                        //
                        if ($strong == TRUE) {
                            $hash = substr($hash, 0, $length);
                        }

                        // encode the hash
                        $hash = urlencode($hash);

                        // set the user info
                        $user->setResetToken($hash);
                        $user->setResetAt(new \DateTime());

                        // grab the entity manager
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($user);
                        $em->flush();

                        //
                        $message = \Swift_Message::newInstance()
                            ->setSubject('Password Reset')
                            ->setFrom('no-reply@example.com')
                            ->setTo($user->getEmail())
                            ->setBody($this->renderView('DLAdminBundle:PasswordReset:reset-email.txt.twig', array('fullName' => $user->getFullName(), 'resetToken' => $user->getResetToken())))
                        ;

                        // fire out the message
                        $this->get('mailer')->send($message);

                    // if not - no problem
                    } catch (UsernameNotFoundException $e) {

                    }
                }

                // set the flash message
                $this->get('session')->getFlashBag()->add('success', 'Your request has been processed. If the username you provided is valid, and the you account enabled, we will have sent an email for you to complete the reset process.');

                // redirect to the listing
                return $this->redirect($this->generateUrl('admin_login'));
        }

        //
        return array('form'=>$form->createView());
    }

    /**
     * @Route("/{resetToken}", name="admin_password_reset_with_token")
     */
    public function resetAction(Request $request, $resetToken)
    {
        // try to load the user
        $user = $this->getDoctrine()->getRepository('DLAdminBundle:User')->loadUserByResetToken($resetToken);

        // if we got null then the user was not active or the reset token valid
        if (null === $user) {

            // set the flash message
            $this->get('session')->getFlashBag()->add('error', 'The reset token is invalid.');

            //
            return $this->redirect($this->generateUrl('admin_login'));
        }

        // Here, "public" is the name of the firewall in your security.yml
        $token = new UsernamePasswordToken($user, null, 'admin_area', $user->getRoles());

        $this->get("security.context")->setToken($token);

        // Fire the login event
        // Logging the user in above the way we do it doesn't do this automatically
        $event = new InteractiveLoginEvent($request, $token);
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

        // set the user info
        $user->setResetToken(null);
        $user->setResetAt(null);

        // grab the entity manager
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        // add a flash message
        $this->get('session')->getFlashBag()->add('success', 'Please now change your password.');

        // redirect the user
        return $this->redirect($this->generateUrl('admin_home'));
    }
}
