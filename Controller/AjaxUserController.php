<?php

namespace DL\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\SecurityExtraBundle\Annotation\Secure;
use DL\CoreBundle\Controller\Controller;

/**
 *
 */
class AjaxUserController extends Controller
{
    /**
     * @Secure("ROLE_ADMIN")
     */
    public function getUserAction(Request $request)
    {
        // get the user from the security context
        $user = $this->getUser();

        $userData = array(
            'id'        => $user->getId(),
            'name'      => $user->getFullName(),
            'username'  => $user->getUsername(),
            'email'     => $user->getEmail()
        );

        return new JsonResponse(array(
            'status'=>'OK',
            'message'=>'Authenticated',
            'user'=>$userData
        ));
    }
}
