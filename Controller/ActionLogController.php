<?php

namespace DL\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use DL\CoreBundle\Controller\Controller;
use DL\AdminBundle\Annotation\AdminPrivilegeDefinition;
use DL\AdminBundle\Entity\ActionLogEntry;

/**
 * ActionLogEntry controller.
 *
 * @AdminPrivilegeDefinition(
 *  role="ROLE_ACTION_LOG_ADMIN",
 *  name="Log Viewer",
 *  description="Can view logs of admin actions"
 * )
 */
class ActionLogController extends Controller
{

    /**
     * Lists all ActionLogEntry entities.
     *
     * @Route("logs", name="admin_action_log")
     * @Template()
     * @Secure(roles="ROLE_ACTION_LOG_ADMIN")
     */
    public function indexAction()
    {
        //
        $this->setTemplateVar('area', 'action_log');

        // set the max per page
        $maxPerPage = 10;

        // get the repo
        $repository = $this->getDoctrine()->getRepository('DLAdminBundle:ActionLogEntry');

        // get the qb
        $qb = $repository->createQueryBuilder('e');

        // order the results
        $qb->orderBy('e.createdAt', 'DESC');

        // get the query from the builder
        $query = $qb->getQuery();

        // get the paginator from the container
        $paginator = $this->get('knp_paginator');

        // get the paingation based on the query and inputs
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1),
            $maxPerPage);

        return array(
            'pagination' => $pagination,
        );

    }

}
