<?php

namespace DL\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use DL\CoreBundle\Http\Response\JsonSuccessResponse;
use DL\CoreBundle\Http\Response\JsonErrorResponse;
use DL\CoreBundle\Http\Response\JsonFormErrorsResponse;
use DL\CoreBundle\Controller\Controller;
use DL\AdminBundle\Annotation\AdminPrivilegeDefinition;
use DL\AdminBundle\Entity\User;
use DL\AdminBundle\Form\UserType;

/**
 * User controller.
 *
 * @AdminPrivilegeDefinition(
 *  role="ROLE_USER_ADMIN",
 *  name="User Admin",
 *  description="Can create and manage administrative users"
 * )
 *
 * @Route("/users")
 */
class UserAdminController extends Controller
{
    /**
     * Lists all User entities.
     *
     * @Route("/", name="admin_users")
     * @Template()
     * @Secure(roles="ROLE_USER_ADMIN")
     */
    public function indexAction()
    {
        //
        $this->setTemplateVar('area', 'users');

        return array();
    }

    /**
     * Lists all User entities.
     *
     * @Route("/list-with-role/{role}", name="admin_users_list_with_role")
     * @Template("DLAdminBundle:UserAdmin:list.html.twig")
     * @Secure(roles="ROLE_USER_ADMIN")
     */
    public function listUsersWithRoleAction($role)
    {
        // set the max per page
        $maxPerPage = 10;

        // get the entity manager
        $em = $this->getDoctrine()->getManager();

        // get the query
        $query = $em->createQueryBuilder('u')
                    ->select('u, p')
                    ->from('DL\AdminBundle\Entity\User', 'u')
                    ->leftJoin('u.privileges', 'p')
                    ->where('p.role = :role')
                    ->setParameter('role', $role)
                    ->andWhere('u.isActive = TRUE')
                    ->getQuery();

        // get the paginator from the container
        $paginator = $this->get('knp_paginator');

        // get the paingation based on the query and inputs
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1),
            $maxPerPage
        );

        // set the route as we are rendered in a subrequest
        $pagination->setUsedRoute('admin_users_list_with_role');

        // get all the privileges
        $privileges = $this->getDoctrine()->getManager()->getRepository('DLAdminBundle:Privilege')->findAll();

        // return the pagination for rendering
        return array(
            'pagination' => $pagination,
            'privileges' => $privileges,
        );
    }

    /**
     * Lists all User entities.
     *
     * @Route("/list", name="admin_users_list")
     * @Template()
     * @Secure(roles="ROLE_USER_ADMIN")
     */
    public function listAction()
    {
        // set the max per page
        $maxPerPage = 10;

        // get the entity manager
        $em = $this->getDoctrine()->getManager();

        // create the query
        $query = $em->createQuery('SELECT u FROM DLAdminBundle:User u');

        // get the paginator from the container
        $paginator = $this->get('knp_paginator');

        // get the paingation based on the query and inputs
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1),
            $maxPerPage
        );

        // set the route as we are rendered in a subrequest
        $pagination->setUsedRoute('admin_users_list');

        // get all the privileges
        $privileges = $this->getDoctrine()->getManager()->getRepository('DLAdminBundle:Privilege')->findAll();

        // return the pagination for rendering
        return array(
            'pagination' => $pagination,
            'privileges' => $privileges,
        );
    }

    /**
     * Displays a form to create a new User entity.
     *
     * @Route("/new", name="admin_users_new")
     * @Template()
     * @Secure(roles="ROLE_USER_ADMIN")
     */
    public function newAction()
    {
        $entity = new User();
        $form   = $this->createForm(new UserType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/create", name="admin_users_create")
     * @Method("POST")
     * @Template("DLAdminBundle:UserAdmin:new.html.twig")
     * @Secure(roles="ROLE_USER_ADMIN")
     */
    public function createAction(Request $request)
    {
        $entity  = new User();
        $form = $this->createForm(new UserType(), $entity);
        $form->bind($request);

        //
        if ($form->isValid()) {

            $password 	= $form->get('password')->getData();

            $factory 	= $this->get('security.encoder_factory');
            $encoder 	= $factory->getEncoder($entity);
            $password 	= $encoder->encodePassword($password, $entity->getSalt());

            $entity->setPassword($password);

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            //
            $msg = sprintf('The user %s has been created', $entity->getUsername());

            // log it
            $this->container->get('dl.admin.logger')->log('Admin Users', $msg);

            //
            return new JsonSuccessResponse($msg);

        } else {

            return new JsonFormErrorsResponse($form);
        }
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="admin_users_edit")
     * @Template()
     * @Secure(roles="ROLE_USER_ADMIN")
     */
    public function editAction(User $user)
    {
        $form = $this->createForm(new UserType($user->getId()), $user);

        return array(
            'entity'      => $user,
            'form'   => $form->createView(),
        );
    }

    /**
     * Edits an existing User entity.
     *
     * @Route("/{id}/update", name="admin_users_update")
     * @Method("POST")
     * @Template("DLAdminBundle:UserAdmin:edit.html.twig")
     * @Secure(roles="ROLE_USER_ADMIN")
     */
    public function updateAction(User $entity, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        // grab the original encrypted password
        $originalHashedPassword = $entity->getPassword();

        $form = $this->createForm(new UserType($entity->getId()), $entity);
        $form->bind($request);

        if ($form->isValid()) {

            // grab the new password
            $newPassword = $form->get('password')->getData();

            // if the user has entered a new password
            if (!empty($newPassword)) {
                $factory 		= $this->get('security.encoder_factory');
                $encoder	 	= $factory->getEncoder($entity);
                $newPassword 	= $encoder->encodePassword($newPassword, $entity->getSalt());
                $entity->setPassword($newPassword);

            } else {
                $entity->setPassword($originalHashedPassword);
            }

            $em->flush();

            $msg = sprintf('The user %s has been updated', $entity->getUsername());

            // log it
            $this->container->get('dl.admin.logger')->log('Admin Users', $msg);

            // if this is the current user
            if ($entity === $this->get('security.context')->getToken()->getUser()) {

                // generate a new token for the entity
                $token = new \Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken(
                    $entity,
                    null,
                    'admin_area',
                    $entity->getRoles()
                );

                // set the token for this user
                $this->container->get('security.context')->setToken($token);

                // refresh the user
                $em->refresh($this->get('security.context')->getToken()->getUser());
            }

            return new JsonSuccessResponse($msg);

        } else {

            return new JsonFormErrorsResponse($form);
        }
    }

    /**
     * Toggles the enabled flag true or false
     *
     * @Route("/{id}/toggle_enabled", name="admin_users_toggle_enabled")
     * @Secure(roles="ROLE_USER_ADMIN")
     */
    public function toggleEnabledAction(Request $request, User $user)
    {

        // dont allow changing of the current user
        if ($user->getId() === $this->getUser()->getId()) {
            return new JsonErrorResponse('You cannot disable the current user');
        }

        //
        $newState = $user->isEnabled() == true ? false : true;
        $user->setIsActive($newState);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        $msg = sprintf('%s user %s', $user->getEnabledString(), $user->getUsername());

        // log it
        $this->container->get('dl.admin.logger')->log('Admin Users', $msg);

        return new JsonSuccessResponse($msg);
    }

    /**
     * Delete the user
     *
     * @Route("/{id}/delete", name="admin_users_delete")
     * @Method("DELETE")
     * @Secure(roles="ROLE_USER_ADMIN")
     */
    public function deleteAction(Request $request, User $user)
    {

        if ($user->getId() === $this->getUser()->getId()) {
            return new JsonErrorResponse('You cannot delete the current user');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        $msg =  sprintf('Deleted user %s', $user->getUsername());

        // log it
        $this->container->get('dl.admin.logger')->log('Admin Users', $msg);

        return new JsonSuccessResponse($msg);
    }

}
