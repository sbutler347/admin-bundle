<?php

namespace DL\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use DL\CoreBundle\Controller\Controller;
use DL\AdminBundle\Form\AdminUserProfileType;

/**
 *
 */
class ProfileController extends Controller
{
    /**
     * Show the user edit profile form
     *
     * @Route("/profile", name="admin_user_profile")
     * @Template("DLAdminBundle:Profile:profile.html.twig")
     * @Secure("ROLE_ADMIN")
     */
    public function indexAction(Request $request)
    {
        // get the user from the security context
        $entity = $this->getUser();

        // grab the original encrypted password
        $originalHashedPassword = $entity->getPassword();

        // create the profile form
        $form = $this->createForm(new AdminUserProfileType, $entity);

        // if this is a form submission
        if ($request->getMethod() == 'POST') {

            $form->bind($request);

            if ($form->isValid()) {

                // grab the new password
                $newPassword = $form->get('password')->getData();

                // if the user has entered a new password
                if (!empty($newPassword)) {

                        $factory 		= $this->get('security.encoder_factory');
                        $encoder	 	= $factory->getEncoder($entity);
                        $newPassword 	= $encoder->encodePassword($newPassword, $entity->getSalt());
                        $entity->setPassword($newPassword);

                } else {
                    $entity->setPassword($originalHashedPassword);
                }

                $em = $this->getDoctrine()->getManager();
                $em->flush();

                //
                $this->container->get('dl.admin.logger')->log('Admin Users', 'Updated their profile');

                // set the flash message
                $this->get('session')->getFlashBag()->add('success', 'Your changes have been saved.');

                // redirect the user
                return $this->redirect($this->generateUrl('admin_user_profile'));
            }
        }

        //
        return array(
                'entity'=> $entity,
                'form'  => $form->createView()
        );

    }
}
