

	/* ==========================================================================
	  	Collapse / Expand sections of tables
	   ========================================================================== */
		
		$(function() {
			
			var $collapsibleTable = $('.table-collapsible');
			var $sections = 	$collapsibleTable.find('tr.section td.collapse');
			var $pages = 		$collapsibleTable.find('tr.page td.collapse');
			
			$sections.each( function() {
				
				$(this).on('click', function(e) {
				
					e.preventDefault();
					var $row = $(this).closest('tr');
					
					if( $row.next('.page').is(':visible') ) {
						
						$row.nextAll('.page, .content').hide();
						$row.nextAll('.page').find('td.collapse a').addClass('expand');
						$row.find('td.collapse a').addClass('expand');
					
					} else {
						
						$row.nextAll('.page').show();
						$row.find('td.collapse a').removeClass('expand');
					
					}
					
				});
			
			});
			
			$pages.each( function() {
				
				$(this).on('click', function(e) {
					
					e.preventDefault();
					var $row = $(this).closest('tr');
					
					if( $row.next('tr.content').is(':visible') ) {
						
						$row.nextUntil('tr.page').hide();
						$row.find('td.collapse a').addClass('expand');
					
					} else {
						
						$row.nextUntil('tr.page').show();
						$row.find('td.collapse a').removeClass('expand');
					
					}
				
				});
				
			});
			
			
			$collapsibleTable.find('tbody tr:not(.section)').hide();
		
		})
