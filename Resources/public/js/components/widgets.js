

	/* ==========================================================================
	   Jquery UI Defaults
	   ========================================================================== */

		$(function() {


		/* ==========================================================================
		   Dialog Options and Configurations
		   ========================================================================== */

			// Dialog needs a wrapper to run perspective CSS animations from.
			$('body').prepend('<div class="ui-dialog-perspective-wrapper"></div>');

			$.extend( $.ui.dialog.prototype.options, {
				modal: true, draggable: false,
				open: function() {
					// use css to run animation instead of JS.
					$(this).parents('.ui-dialog').removeClass('out').addClass('in');
					$('#container').addClass("blur");
					$('.ui-widget-overlay').fadeIn();
				}
			});

			// function to close dialog using css animations.
			$.fn.animateDialogClose = function() {
				var $dialog = $(this);
					// use css to run animation instead of JS.
					$dialog.parents('.ui-dialog').removeClass('in').addClass('out');
					$('.ui-widget-overlay').fadeOut();
					$('#container').removeClass("blur");
					// delay close so we actually see the animation.
					setTimeout( function() { $dialog.dialog('close'); }, 500);

				return $dialog;

			};




		/* ==========================================================================
		   Always Animate Sliders
		   ========================================================================== */

			$.extend( $.ui.slider.prototype.options, {
				 animate: true
			});






		/* ==========================================================================
		   Datepicker Defaults
		   ========================================================================== */

			// defaults for datepicker
			$.datepicker.setDefaults({
				dateFormat: "yy-mm-dd",
				defaultDate: new Date(),
				showOn: 'both',
				buttonImage: "/bundles/dladmin/img/vendor/fugue/icons/calendar-month.png",
				buttonImageOnly: true
			});




			dlInitJSFormElements();



		});







	function dlInitJSFormElements() {


			$(':checkbox:not(.clean)').chkbox();




			$('select').each( function() {

				var $this = $(this);
				var classes = $this.attr('class');

				$this.select2({ minimumResultsForSearch: 14 });
				$this.trigger('change');

			});


			$('.select-tags').select2('destroy');
			$('.select-tags').each( function() {

				var $this = $(this);

				$this.select2({
					tags: "",
					tokenSeparators: [",", " "]
				});

				$this.select2("container").find("ul.select2-choices").sortable({
					containment: 'parent',
					start: function() { $tags.select2("onSortStart"); },
					update: function() { $tags.select2("onSortEnd"); }
				});
			});


			$('.file-input input[type=file]').each( function() {

				var file = $(this).val();
				if( file === "" ) { file = $(this).closest('.formAnswer').find('.address-bar .filename').text(); }
				$(this).closest('.formAnswer').find('.address-bar .filename').text( file );

				$(this).off('change').on('change', function() {
					var file = $(this).val();
					$(this).closest('.formAnswer').find('.address-bar .filename').text( file );
				});

			});


            // destroy select2 so that asmselect can be used instead
            $('.asmSelectAdd').select2('destroy');

            $('.asmSelectAdd').hide();

            // init asm selec if the plugin exists
            if ($.fn.asmSelect) {
                $(".asmSelectAdd").each(function(){
                    $this = $(this);

                    if (!$this.hasClass('asmselect_init')){
                    var asm = $this.asmSelect({
                            animate: true,
                            highlight: true,
                            sortable: true
                    });
                    }
                });

                // this is only if add is used
                $(".asmselect_add_btn").click(function() {
                    var value = $(".asmselect_add_value").val();
                    var $option = $("<option></option>").text(value).attr("selected", true);
                    $(".asmSelectAdd").append($option).change();
                    $(".asmselect_add_value").val('');

                    return false;
                });
            }


			// only apply datepicker to desktop devices. Why? Mobiles have good datepickers.
			// not fool-proof, but pretty close.
			if( !Modernizr.touch || screen.width > 680 ) {
				$('input[type=date], input.date').each( function() {

					var $this = $(this);

					$this.prop('type', 'text');
					$this.datepicker('destroy');
					$this.datepicker();

					if ( $this.val() == "" ) {
						$this.datepicker('setDate', new Date());
					}

				});
			}


			// create sliders
			$('.ui-slider').slider('destroy').slider();



			// activate nice image previews
			var $zoomEls = $(".background-image-preview, .preview-image");
			$zoomEls.on("click", function() {
				imageZoom( $(this) );
			});





		/* ==========================================================================
		   qTip Tooltip applier
		   ========================================================================== */

			// apply tooltips to any element with 'class=tooltip'
			// dont do it on touchscreens.
			if( !Modernizr.touch ) {

				$.fn.qtip.defaults = $.extend(true, {}, $.fn.qtip.defaults, {
					attr: 'title',
					position: { viewport: true, my: 'top center', at: 'bottom center', adjust: { y: 5, x: 5 } },
					style: { tip: { corner: true, width: 10 } },
					show: { event: 'mouseenter focus' },
					hide: { event: 'blur unfocus mouseleave' }
				});

				$('.tooltip').qtip('destroy').qtip();

			};




		/* ==========================================================================
		   prevent default on collapse toggle buttons
		   ========================================================================== */

			$('button[data-toggle=collapse]').on('click', function(e) {
				e.preventDefault();
			});

			$('button[data-toggle=collapse]').each( function() {

				var icon = $(this).find('.icn').data('icon');
				var alticon = $(this).find('.icn').data('alt-icon');
				var $icon = $(this).find('.icn');

				$icon.addClass( icon );

				$(this).on('click', function() {
					if( $icon.hasClass( icon ) ) {
						$icon.removeClass( icon ).addClass( alticon );
					} else {
						$icon.removeClass( alticon ).addClass( icon );
					}
				});

			});






		}






