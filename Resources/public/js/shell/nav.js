/* ==========================================================================
   Navigation Javascript
   ========================================================================== */

$(function() {

    $('nav .sub').hide();

    // find an active element and see if it has a parent of sub, if so add active to it
    var $active = $('nav li.active');

    // if we have an active li
    if ($active.length > 0) {

        // find if there is a parent ul.subnav
        var $parentUl = $active.parentsUntil('ul.subnav');

        if ($parentUl.length > 0) {

            // add the active class
            $parentUl.parent('li').addClass('active');

            // show the subnav
            $parentUl.show();
        }
    }

    $('nav .item > a').on('click', function(e) {

        var $_this = $(this).parent('.item');

        // only collapse navigation if it's in wide-screen.
        if( $('nav').width() > 100 ) {

            // only prevent clicking on multi-level navigation
            if( $_this.find('.sub').length > 0 ) {
                e.preventDefault();
            }

            // if this item is active/expanded...
            if( $_this.hasClass('active') ) { 
                $_this.find('.sub').stop(true,true).slideUp();
                $_this.removeClass('active');
            } else { 
                $('nav .sub').not( $_this.find('.sub')).slideUp();
                $('nav .item').not( $_this ).removeClass('active');
                $_this.find('.sub').stop(true,true).slideDown();
                $_this.addClass('active');
            }
        }

    });

    // add a solo onto nav items with no children
    $('nav .item').each( function() {

        var $_this = $(this);
        if( $_this.find('.subitem').length == 0 ) {
            $_this.addClass('solo');	
        }

        if( $_this.hasClass('active') ) { 
            $_this.find('.sub').show();
        }
    });
});
