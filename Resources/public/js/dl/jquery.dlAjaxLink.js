/**
 *
 */
$.fn.dlAjaxLink = function(e) {

    e.preventDefault();

    var $this       = $(this);
        
    var url         = $this.attr('href');
    var $row        = $this.parents('tr');
    var callback    = $this.data('ajaxLinkSuccessCallback');

    var data = { 
        'id': $this.data('entityId'),
    };

    // http://stackoverflow.com/questions/15772920/firefox-exception-javascript-component-does-not-have-a-method-named-available
    // hard to believe but the var _method containing DELETE is causing ff to complain badly. hence the JSON.stringify below wrapping data

    var jqxhr = $.ajax({
        url: url,
        type: 'GET',
        data: JSON.stringify(data), 
        cache: false, 
        contentType: false,
        processData: false, 
        success: function(data, status, xhr) {

            if (-1 !== xhr.getResponseHeader('Content-Type').indexOf('json')) { 
                if (data.hasOwnProperty('message')) { 
                    // 
                    growl(data.message, {style:'success'});

                } else {
                    growl('Item has been <strong>updated</strong>', {style:'success'});
                }

                try { 
                    callback();
                } catch (error) { 

                }
            }
        }, 
        error: function(e) {
            alert('An error occured. Please try again');
        }  
    });
}
