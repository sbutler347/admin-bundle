/**
 * @TODO perhaps add third param for callback (e.g. listEntities)
 */
$.fn.dlToggleEntityState = function(e , message) {
	
    e.preventDefault();

    var $this = $(this),
    	url = $this.attr('href'),
		state,
		style = "warning";
	
	if ( typeof(message) === "undefined" ) {
		var message = "This item has been set to <strong>__STATE__</strong>";
	}
	
	// replace the active/inactive icon with spinner
	$this.swapOutForSpinner();

    // 
    $.get(url, function(data) {

        if (data.status === 'OK') {

            state = '';

			// if this item is active 
			if ( $this.hasClass('active') || $this.hasClass('enabled') ) {
		
				// change it's state to inactive
				$this.removeClass('active enabled').addClass('disabled').text('Active');
				$this.parents('tr').removeClass('active enabled').addClass('disabled');
				state = "inactive";
		
			} else if ( $this.hasClass('inactive') || $this.hasClass('disabled') ) { 
		
				// change it's state to active
				$this.removeClass('inactive disabled').addClass('enabled').text('Inactive');
				$this.parents('tr').removeClass('disabled').addClass('enabled');
				state = "active";
				style = "success";
		
			}
			
            if (data.hasOwnProperty('message')) { 
                message = data.message;
            } else { 
    			message = message.replace( "__STATE__" , state);
            } 

        } else {
			
           	message = 'An error occured. Please try again';
			style = 'error';
			
        }
		
		// display the growl notification
		growl( message, { style: style });
		
		// swap it back in
   		$this.swapInForSpinner();
	
    });
    
};
