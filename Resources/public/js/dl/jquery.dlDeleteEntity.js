/**
 * @TODO perhaps add third param for callback (e.g. listEntities)
 * @TODO take a confirmDialogMessage and inject the dialog directly then remove it
 */
$.fn.dlDeleteEntity = function(e, options) {
    e.preventDefault();

    var $this = $(this);

    if (options.hasOwnProperty('confirmDialog')) {
  
        $confirm = options.confirmDialog;

    	if (options.hasOwnProperty('callback')) {
			callback = options.callback;
		} else {
			callback = false;
		}
        
		$confirm.dialog({
            modal: true,
            buttons: {

                "ok": {
                    'text':'Ok', 'class':'btn red',
                    click: function() { 
                        $.fn.dlAjaxDeleteEntity($this, callback);
                        $(this).animateDialogClose(); 
                    }
                },  
                "cancel": {
                    'text':'Cancel', 'class':'btn',
                    click: function() { 
                        $(this).animateDialogClose(); 
                    }
                }   
            }   

        }).dialog('widget').appendTo('.ui-dialog-perspective-wrapper');

        // 
    } else { 
        $.fn.dlAjaxDeleteEntity($this, callback);
    }
};

$.fn.dlAjaxDeleteEntity = function($this, callback) {
    
    var url = $this.attr('href');
    var $row = $this.parents('tr');

    var data = { 
        'id': $this.data('entityId'),
    };

    // http://stackoverflow.com/questions/15772920/firefox-exception-javascript-component-does-not-have-a-method-named-available
    // hard to believe but the var _method containing DELETE is causing ff to complain badly. hence the JSON.stringify below wrapping data

    var jqxhr = $.ajax({
        url: url,
        type: 'DELETE',
        data: JSON.stringify(data), 
        cache: false, 
        contentType: false,
        processData: false, 
        success: function(data, status, xhr) {
            if (-1 !== xhr.getResponseHeader('Content-Type').indexOf('json')) { 

                if (data.status === 'OK') {

                    if (data.hasOwnProperty('message')) { 
                        growl(data.message, {style:'success'});
                    } else {
                        growl('Item has been <strong>deleted</strong>', {style:'success'});
                    }

                    // fade out the row and remove the dialog
                    $row.fadeOut().attr('data-destroy','_destroy');
					
					if (callback) {
						callback();
					}

                    // show the message 
                } else {
                    if (data.hasOwnProperty('message')) { 
                        growl(data.message, {style:'error'});
                    } else {
                        growl('An error occured. Please try again', {style:'error'});
                    }
                }
            }
        }, 
        error: function(e) {
            alert('An error occured. Please try again');
        }  
    });
}
