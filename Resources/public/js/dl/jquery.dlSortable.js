/**
 * Uses jquery ui sortable library
 */
function dlSortable(tableId) {

    var $table      = $('#' + tableId);
    var $tableBody  = $('#' + tableId + ' tbody');
    var sortUrl     = $table.data('sortUrl');

    // check the table exists
    if ($table.length === 0) {
        console.log("Table " + tableId + ' does not exist');
        return;
    }

    // check we have setup a sort url
    if (typeof sortUrl === 'undefined') {
        console.log("Sort url data attribute is not defined on " + tableId);
        return;
    }

    // create the sortable
    $tableBody.sortable({

        update: function( event, ui ) {

            // Disable from further sorting
            $tableBody.sortable("disable");

            var newIndex    = ui.item.index();
            var order       = $tableBody.sortable('toArray');
            var itemId      = $('#' + order[newIndex]).data('entity-id');

            var data        = {
                newPosition: newIndex,
                itemID: itemId,
                order: order
            }

            var jqxhr       = $.ajax({
                url: sortUrl,
                type: 'POST',
                data: JSON.stringify(data),
                cache: false,
                contentType: false,
                processData: false,
                success: function(data, status, xhr) {

                    // re-enable the sorting
                    $tableBody.sortable("enable");

                    if (data.hasOwnProperty('message')) {
                        //
                        growl(data.message, {style:'success'});

                    } else {
                        growl('Order has been <strong>updated</strong> succesfully', {style:'success'});
                    }

                    try {
                        var order = $tableBody.sortable('toArray');

                        for (var i=0; i < order.length; i++) {
                            $('#' + order[i] + ' .positionfield').html(i);
                        }

                    } catch (error) {
                    }
                },
                error: function(e) {
                    alert('An error occured. Please refresh and try again.');
                }
            });
        }
    });
}
