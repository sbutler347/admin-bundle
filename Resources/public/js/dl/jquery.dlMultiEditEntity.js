/**
 * Provides ability to add a new 'edit' tab and content tab panel
 */
$.fn.dlMultiEditEntity = function(e, tabsBaseName, entityType, icon) {

    e.preventDefault();

    var $this =  $(this);

    // grab the url 
    var url                 = $this.attr('href');
    var entityId            = $this.data('entityId');
    var entityName          = $this.data('entityName');

    var tabsUl              = 'ul.' + tabsBaseName + '-tabs';
    var tabsContentDiv      = 'div.' + tabsBaseName + '-tabs-content';
    var li                  = entityType + 'EditEntityNavLi' + entityId;
    var hLi                 = '#' + li;
    var tab                 = entityType + 'EditEntityTab' + entityId;
    var hTab                = '#' + tab;
	
    // if this tab already exists then trigger it
    if ($(hLi).length) {
        $(hLi + ' a').trigger('click');
        return false;
    } 

    // add in the tab nav 
    $(tabsUl).append('<li id="' + li + '" class="with-close"><a href="' + hTab + '" data-toggle="tab"><i class="icn i-'+ icon +'"></i>' + entityName + '</a><span href="" class="multi-close-tab" data-remove-li="' + hLi + '" data-remove-tab="' + hTab + '"><i class="icn i-cross-small"></i></span></li>');

    // add in the tab container to the dom
    $(tabsContentDiv).append('<div class="tab-pane" id="' + tab + '" data-tab-id="' + li + '">Please wait. Loading...<i class="spinner tiny"></i></div>');

//    $.ajaxSetup({cache: true})
    // load in the data
    $(hTab).load(url, function(response, status, xhr) {
        // initialize any js form elements
        dlInitJSFormElements();

        if (status === "error") {
            alert('Sorry but there was an error: ' + xhr.statusText);
        }
    });

    // click the tab to active 
    $(hLi + ' a').trigger('click');
};

// add the specific handler for the close tab
$(document).ready(function() {

    $(document).on('click', 'span.multi-close-tab', function(e) { 

        e.preventDefault();

        var $this   = $(this);
        var hLi     = $this.data('removeLi');
        var hTab    = $this.data('removeTab');
        
        // find the parent ul then the first tab and click that
        var $activeTabRow = $(hLi).closest(".nav-tabs");

        // remove the nav item
        $(hLi).remove();

        // remove the tab itself (and events) 
        $(hTab).remove();

        // only trigger the "home" tab if you close the tab that is active.
        if( $activeTabRow.children("li.active").length < 1 ) {
            $activeTabRow.find('li:first a').trigger('click');
        }

    });
});
