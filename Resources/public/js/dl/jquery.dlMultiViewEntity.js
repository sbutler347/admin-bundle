/**
 * Provides ability to add a new 'edit' tab and content tab panel
 */
$.fn.dlMultiViewEntity = function(e, tabsBaseName, entityType, icon) {

    e.preventDefault();

    var $this =  $(this);

    // grab the url 
    var url                 = $this.attr('href');
    var entityId            = $this.data('entityId');
    var entityName          = $this.data('entityName');

    var tabsUl              = 'ul.' + tabsBaseName + '-tabs';
    var tabsContentDiv      = 'div.' + tabsBaseName + '-tabs-content';
    var li                  = entityType + 'ViewEntityNavLi' + entityId;
    var hLi                 = '#' + li;
    var tab                 = entityType + 'ViewEntityTab' + entityId;
    var hTab                = '#' + tab;
	
    // if this tab already exists then trigger it
    if ($(hLi).length) {
        $(hLi + ' a').trigger('click');
        return false;
    } 

    // add in the tab nav 
    $(tabsUl).append('<li id="' + li + '"><a href="' + hTab + '" data-toggle="tab"><i class="icn i-'+ icon +'"></i>' + entityName + '</a></li>');

    // add in the tab container to the dom
    $(tabsContentDiv).append('<div class="tab-pane" id="' + tab + '" data-tab-id="' + li + '">Please wait. Loading...<i class="spinner tiny"></i></div>');

    // load in the data
    $(hTab).load(url, function(response, status, xhr) {

        // initialize any js form elements
        dlInitJSFormElements();

        if (status === "error") {
            alert('Sorry but there was an error: ' + xhr.statusText);
        }
    });

    // click the tab to active 
    $(hLi + ' a').trigger('click');
};
