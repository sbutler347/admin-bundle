//Create namespace
window.DL = window.DL || {};

DL.collection = function(formId, name) {

    if (name == null) {
        name = "__name__";
    }

    var collection = jQuery('.dl-collection_' + formId);

    // add to each element 
    collection.each(function(k, v) {

        var options   = jQuery(this).data('options');
        var prototype = jQuery(this).data('prototype');

        // fix mopa bundle
        if(prototype == '') {
            prototype = jQuery(this).closest('div[data-prototype]').data('prototype');
        }

        var container = jQuery('#dl-collection-container-' + options.id);
        var buttonAdd = jQuery('#dl-collection-button-add-' + options.id);
        var elementIdx = jQuery('#dl-collection-container-' + options.id + ' > fieldset').length;
        var idx = elementIdx;

        // 
        jQuery.each(container.children('fieldset'), function(k, v) {
            var currentIdx = jQuery(this).data('idx');
            if(currentIdx >= elementIdx){
                idx = currentIdx + 1;
            }
        });
       
        // 
        jQuery(document).on('click', '#dl-collection-button-add-' + options.id, function(event) {
            event.preventDefault();
            
            var re = new RegExp(name,"g");
            var html = prototype.replace(re, idx);
			
            container.append('<fieldset id="dl_fieldset_'+ idx +'" data-idx="'+ idx +'" class="'+ options.fieldset_class +'">' + html + '<button class="'+ options.remove_button_class +' dl-collection-button-remove">'+ options.remove_button_text + '</button></fieldset>');
           
            var onAddEvent = jQuery.Event('dl_form.collection.onAdd');
            onAddEvent.options = options;
            onAddEvent.idx = idx;
            jQuery(document).trigger(onAddEvent);

            idx++;
            if (jQuery('#dl-collection-container-' + options.id + ' > fieldset').length > 0) {
                jQuery('#dl-collection-empty-text-' + options.id).hide();
            }
        });
        

        jQuery(document).on('click', '.dl-collection-button-remove', function(event) {
            event.preventDefault();
            
            jQuery(this).parent().fadeOut(function() {
                jQuery(this).remove();

                var onRemoveEvent = jQuery.Event('dl_form.collection.onRemove');
                onRemoveEvent.options = options;

                jQuery(document).trigger(onRemoveEvent);

                if(jQuery('#dl-collection-container-' + options.id + ' > fieldset').length == 0) {
                    jQuery('#dl-collection-empty-text-' + options.id).fadeIn();
                }
            });

        });
    });
};
