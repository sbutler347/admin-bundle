//Create namespace
window.DL = window.DL || {};

DL.metadata = function(formId){
    var metadata = jQuery('.dl-metadata_' + formId);
    
    // Loop through each instance of metadata
    metadata.each(function(k,v){
        var options = jQuery(this).data('options');
        var prototype = jQuery(this).data('prototype');
        
        if(prototype == ''){
            prototype = jQuery(this).closest('div[data-prototype]').data('prototype');
        }
		
        var container = jQuery('#dl-metadata-container-' + options.id);
        var buttonAdd = jQuery('#dl-metadata-button-add-' + options.id);
        var elementIdx = jQuery('#dl-metadata-container-' + options.id + ' > fieldset').length;
        var idx = elementIdx;

        jQuery.each(container.find('fieldset'),function(k,v){
            var currentIdx = jQuery(this).data('idx');
            if(currentIdx >= elementIdx){
                idx = currentIdx + 1;
            }
        });
        
        // Adds on click listener for add button
        jQuery(document).on('click', '#dl-metadata-button-add-' + options.id, function(event){
            event.preventDefault();

            // replace __name__ with the field id
            var html = prototype.replace(/__name__/g, idx);

            // append an add button and the html from the prototype
            container.append('<fieldset id="dl_fieldset_'+ idx +'" data-idx="'+ idx +'" class="'+ options.fieldset_class +'">' + html + '<button class="'+ options.remove_button_class +' dl-metadata-button-remove">'+ options.remove_button_text + '</button></fieldset>');
            
            // set inherit to checked by default
            $('#' + options.id + '_' + idx + '_inherit').attr('checked','checked'); 
            // set the checkbox to fancy js styling
            $('#dl_fieldset_'+ idx +' input:checkbox:not(.clean)').chkbox();
            
            var fields = new Array();
			
            fields.push(options.id + "_" + idx + "_name");
			fields.push(options.id + "_" + idx + "_property");
			
            // run the initSelect2 function on th two select2 fields
			fields.forEach(function(field){
			$("#" + field).each( function() {
				var $this = $(this);
				initSelect2($this);
			});
			});
            
			idx++;
            if(jQuery('#dl-metadata-container-' + options.id + ' > fieldset').length > 0){
                jQuery('#dl-metadata-empty-text-' + options.id).hide();
            }
			
        });
        
        // On click listener for remove button
        jQuery(document).on('click', '.dl-metadata-button-remove', function(event){
            event.preventDefault();
            
            jQuery(this).parent().fadeOut(function(){
                jQuery(this).remove();

                if(jQuery('#dl-metadata-container-' + options.id + ' > fieldset').length == 0){
                    jQuery('#dl-metadata-empty-text-' + options.id).fadeIn();
                }

            });
        });
    });

    // reinitialise all select2s used by metadata so that default options are included
    $('.select-meta').select2('destroy');
    $('.select-meta').each( function() {
        var $this = $(this);
        initSelect2($this);
    });

};

/**
 * reinitialises select as select2 with defautls included
 *
**/
function initSelect2($this){
    
    var data = $this.data('items');
	var exploded = data.split('|');
    
    var nameDefaults = [
        'title',
        'description',
        'twitter:card',
        'twitter:url',
        'twitter:title',
        'twitter:description',
        'twitter:image',
        'msapplication-TileImage',
        'msapplication-TileColor'
    ];

    var propDefaults = [
        'og:title',
        'og:description',
        'og:type',
        'og:url',
        'og:image',
        'og:site_name',
        'fb:app_id'
    ];
    
    // If the select is the name select use name defaults
    if ($($this).hasClass('meta-name')) {
        var defaults = nameDefaults;
    }

    // If the select is the property select use the property defaults
    if ($($this).hasClass('meta-prop')) {
        var defaults = propDefaults;
    }
    
    // add all defaults to the exploded list
    for (var j = 0; j < defaults.length; j++) {
        if ($.inArray(defaults[j], exploded) == -1) {
            exploded.push(defaults[j]);
        }
    }

    var newData = [];
	var selected;

    // Add each item from exploded to the newData array
	for (var i =0; i<exploded.length; i++) {
		var row = {
			"text": exploded[i] 
		}
		newData.push(row);
	}

    // reinitialise the select2 with the defaults added
	$this.select2({
    	createSearchChoice:function(term, data) { if ($(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
	    data: newData,
    	id : function(object) {
        	return object.text;
	    }
	});

    // This sets the correct value on the select2
	$this.select2('val', $this.val());
}

