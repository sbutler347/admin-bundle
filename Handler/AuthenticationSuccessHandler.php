<?php

namespace DL\AdminBundle\Handler;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use DL\CoreBundle\Http\Response\JsonSuccessResponse;

class AuthenticationSuccessHandler implements AuthenticationSuccessHandlerInterface
{
   private $container;

   /**
    * AuthenticationSuccessHandler constructor
    * @param RouterInterface   $router
    * @param EntityManager     $em
    * @param LoggerInterface   $logger
    * @param LoggerInterface   $connectionLogger
    */
    public function __construct(ContainerInterface $container)
       {
        $this->container = $container;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {

        // set the last login at timestamp
        $token->getUser()->setLastLoginAt(new \DateTime());

        // log this connection
        $this->container->get('dl.admin.logger')->log('Admin Login', 'Logged in from ' . $request->getClientIp());

        // flush the entity
        $this->container->get('doctrine')->getManager()->flush();

        if ($request->isXmlHttpRequest()) {

            $user = $token->getUser();

            $userData = array(
                'id' => $user->getId(), 
                'name' => $user->getFullName(), 
                'username' => $user->getUsername(), 
                'email' => $user->getEmail()
            );

            return new JsonResponse(array('status'=>'OK','message'=>'Authenticated', 'user'=>$userData));
        }

        // set the default url
        $url = $this->container->get('router')->generate('admin_home');

        // set the target url if available
        if ($request->getSession()->get('_security.admin_area.target_path')) {
            $url = $request->getSession()->get('_security.admin_area.target_path');
        }

        return new RedirectResponse($url);
    }

}
