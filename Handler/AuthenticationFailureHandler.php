<?php

namespace DL\AdminBundle\Handler;

use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use DL\CoreBundle\Http\Response\JsonErrorResponse;

class AuthenticationFailureHandler implements AuthenticationFailureHandlerInterface
{
   private $container;

   /**
    * AuthenticationSuccessHandler constructor
    * @param RouterInterface   $router
    * @param EntityManager     $em
    * @param LoggerInterface   $logger
    * @param LoggerInterface   $connectionLogger
    */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        if ($request->isXmlHttpRequest()) {
            return new JsonErrorResponse($exception->getMessage());
        }

         // set authentication exception to session
         $request->getSession()->set(SecurityContextInterface::AUTHENTICATION_ERROR, $exception);

         return new RedirectResponse($this->container->get('router')->generate('admin_login'));
    }

}
