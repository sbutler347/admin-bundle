<?php

namespace DL\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    private $formTypeName = 'admin_user';

    public function __construct($userId = null)
    {
        if (null !== $userId) {
            $this->formTypeName = $this->formTypeName . '_' . $userId;
        }
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('email')
            ->add('fullName', null, array(
                'label' => 'Full name'
            ))
            ->add('mobile')
            ->add('bio', null, array(
                'required' => false,
            ))
            ->add('photoFile', null, array(
                'required' => false,
                'label' => 'Photo',
            ))
            ->add('password', 'repeated', array(
                'type'=>'password',
                'invalid_message' => 'The passwords must match',
                'options' => array('required'=>false),
                'first_options' => array('label' => 'Password'),
                'second_options' => array('label' => 'Confirm Password')
            ))
            ->add('privileges', 'entity', array(
                'class'=>'DLAdminBundle:Privilege',
                'multiple'=>true,
                'expanded'=>true
            ))
            ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DL\AdminBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return $this->formTypeName;
    }
}
