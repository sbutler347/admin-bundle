<?php

namespace DL\AdminBundle\Form\Type;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ReversedTransformer;

/**
 * This class creates ASM Select element
 *
 * @author Scott Pringle
 * @since 1.0
 */
class AsmSelectType extends AbstractType
{
    public function __construct()
    {
    }

    /**
     * (non-PHPdoc)
     * @see \Symfony\Component\Form\AbstractType::buildForm()
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entityManager = $options['em'];
    }

    /**
     * (non-PHPdoc)
     * @see Symfony\Component\Form.AbstractType::buildView()
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['configs'] = $options['configs'];
    }

    /**
     * (non-PHPdoc)
     * @see Symfony\Component\Form.AbstractType::setDefaultOptions()
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {

        $defaultConfigs = array(
            'width' => '300px',
            'allowClear' => true,
            'allowAdd' => false,
        );

        $defaults = array(
            'multiple' => true,
            'expanded' => false,
            'empty_value' => 'select.empty_value',
            'translation_domain' => 'ThraceFormBundle',
            'configs' => $defaultConfigs,
            'attr' =>array( 'title' => "Select Options")
        );

        $normalizers = array(
            'expanded' => function (Options $options, $value) {
                return false;
            },
            'configs' => function (Options $options, $value) use ($defaultConfigs) {
                $configs = array_replace_recursive($defaultConfigs, $value);

                $configs['placeholder'] = $options->get('empty_value');

                if(true === $options->get('multiple') && isset($configs['ajax'])){
                    $configs['multiple'] = true;
                }

                return $configs;
            }
        );

        $resolver->setDefaults($defaults);

        $resolver->setNormalizers($normalizers);
    }

    /**
     * (non-PHPdoc)
     * @see Symfony\Component\Form.AbstractType::getParent()
     */
    public function getParent()
    {
        return 'entity';
    }

    /**
     * (non-PHPdoc)
     * @see Symfony\Component\Form.FormTypeInterface::getName()
     */
    public function getName()
    {
        return 'asmselect';
    }
}
