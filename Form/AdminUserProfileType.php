<?php

namespace DL\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AdminUserProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('username', 'text', array('disabled'=>'true'))
            ->add('email')
            ->add('fullName', null, array(
                    'label' => 'Full name'
                ))
            ->add('mobile')
            ->add('password', 'repeated', array(
                    'type'=>'password',
                    'invalid_message' => 'The passwords must match',
                    'options' => array('required'=>true),
                    'first_options' => array('label' => 'Password'),
                    'second_options' => array('label' => 'Confirm Password')
                ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DL\AdminBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'dl_adminbundle_adminuserprofiletype';
    }
}
