<?php

namespace DL\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * DL\AdminBundle\Entity\User
 * @ORM\Table(name="admin_user")
 * @ORM\Entity(repositoryClass="DL\AdminBundle\Entity\UserRepository")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity(fields="username", message="Username already taken")
 */
class User implements AdvancedUserInterface, EquatableInterface, \Serializable
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30, unique=true)
     * @Assert\NotNull(message="A username is required")
     * @Assert\Length(min="4", minMessage = "Your first name must be at least {{ limit }} characters length")
     * @Assert\Regex(pattern="/^[a-zA-Z]{4,}$/", match=true, message="A username must contain only the letters a-z")
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotNull(message="An email address is required")
     * @Assert\Email(message="The email address '{{ value }}' is not a valid email.", checkMX=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $salt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(name="full_name", type="string", length=255)
     */
    private $fullName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mobile;

    /**
     * @var UploadedFile $photoFile
     *
     * @Assert\NotBlank(groups={"create"})
     * @Assert\File(maxSize="500k", mimeTypes={"image/jpeg", "image/png", "image/gif"}, mimeTypesMessage = "Please upload a valid PNG or JPG image", groups={"create"})
     */
    private $photoFile;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * @ORM\Column(name="bio", type="string", length=255, nullable=true)
     */
    private $bio;

    /**
     * @ORM\ManyToMany(targetEntity="Privilege", inversedBy="users")
     * @ORM\JoinTable(name="admin_user_privilege")
     */
    private $privileges;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="last_login_at", type="datetime", nullable=true)
     */
    private $lastLoginAt;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(name="reset_token", type="string", length=255, nullable=true)
     */
    private $resetToken;

    /**
     * @ORM\Column(name="reset_at", type="datetime", nullable=true)
     */
    private $resetAt;

    /**
     *
     */
    public function __construct()
    {

        // set the user to be active by default
        $this->isActive = true;

        // set the salt for the user
        $this->salt = md5(uniqid(null, true));

        // a holder for the privileges;
        $this->privileges = new ArrayCollection();

    }

    /**
     * @inheritdoc
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @inheritdoc
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @inheritdoc
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @inheritdoc
     */
    public function getRoles()
    {
        // return the privileges
        return array_merge(array('ROLE_USER', 'ROLE_ADMIN'), $this->privileges->toArray());
    }

    /**
     * @inheritdoc
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param  string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Set email
     *
     * @param  string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set salt
     *
     * @param  string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Set password
     *
     * @param  string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set fullName
     *
     * @param  string $fullName
     * @return User
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set mobile
     *
     * @param  string $mobile
     * @return User
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set photoFile
     *
     * @param  string $photoFile
     * @return User
     */
    public function setPhotoFile($photoFile)
    {
        $this->photoFile = $photoFile;

        return $this;
    }

    /**
     * Get photoFile
     *
     * @return string
     */
    public function getPhotoFile()
    {
        return $this->photoFile;
    }

    /**
     * Set photo
     *
     * @param  string $photo
     * @return User
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set bio
     *
     * @param  string $bio
     * @return User
     */
    public function setBio($bio)
    {
        $this->bio = $bio;

        return $this;
    }

    /**
     * Get bio
     *
     * @return string
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * Set isActive
     *
     * @param  boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @inheritdoc
     */
    public function isAccountNonExpired()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function isAccountNonLocked()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function isEnabled()
    {
        return $this->isActive;
    }

    /**
     * Set createdAt
     *
     * @param  \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param  \DateTime $updatedAt
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set lastLoginAt
     *
     * @param  \DateTime $lastLoginAt
     * @return User
     */
    public function setLastLoginAt($lastLoginAt)
    {
        $this->lastLoginAt = $lastLoginAt;

        return $this;
    }

    /**
     * Get lastLoginAt
     *
     * @return \DateTime
     */
    public function getLastLoginAt()
    {
        return $this->lastLoginAt;
    }

    /**
     *
     */
    public function getEnabledString()
    {
        return $this->isActive ? 'active' : 'inactive';
    }

    /**
     * Add privileges
     *
     * @param  DL\AdminBundle\Entity\Privilege $privileges
     * @return User
     */
    public function addPrivilege(\DL\AdminBundle\Entity\Privilege $privilege)
    {
        $this->privileges[] = $privilege;

        return $this;
    }

    /**
     * Remove privileges
     *
     * @param DL\AdminBundle\Entity\Privilege $privileges
     */
    public function removePrivilege(\DL\AdminBundle\Entity\Privilege $privilege)
    {
        $this->privileges->removeElement($privilege);
    }

    /**
     * Get privileges
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getPrivileges()
    {
        return $this->privileges;
    }

    /**
     *
     */
    public function isEqualTo(UserInterface $user)
    {
        return
            md5($user->getUsername()) == md5($this->getUsername());
    }

    /**
     * Bug fix for the php 5.4 serialize method
     */
    public function serialize()
    {
        return json_encode(array(
            $this->id,
            $this->username,
            $this->email,
            $this->password,
            $this->salt,
            $this->privileges,
            $this->fullName,
            $this->mobile,
            $this->createdAt,
            $this->updatedAt,
            $this->isActive
        ));
    }

    /**
     * Bug fix for the php 5.4 serialize method
     */
    public function unserialize($serialized)
    {
       list(
            $this->id,
            $this->username,
            $this->email,
            $this->password,
            $this->salt,
            $this->privileges,
            $this->fullName,
            $this->mobile,
            $this->createdAt,
            $this->updatedAt,
            $this->isActive,
        ) = json_decode($serialized, true);
    }

    /**
     * Set resetToken
     *
     * @param  string $resetToken
     * @return User
     */
    public function setResetToken($resetToken)
    {
        $this->resetToken = $resetToken;

        return $this;
    }

    /**
     * Get resetToken
     *
     * @return string
     */
    public function getResetToken()
    {
        return $this->resetToken;
    }

    /**
     * Set resetAt
     *
     * @param  \DateTime $resetAt
     * @return User
     */
    public function setResetAt($resetAt)
    {
        $this->resetAt = $resetAt;

        return $this;
    }

    /**
     * Get resetAt
     *
     * @return \DateTime
     */
    public function getResetAt()
    {
        return $this->resetAt;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {

        // if a file has been uploaded
         if (null !== $this->photoFile) {

            // generate a unique name for security
            $this->photo = uniqid() . '.' . $this->photoFile->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null !== $this->photoFile) {

            // move to the correct directory
            $this->photoFile->move($this->getUploadRootDir(), $this->photo);

            // we don't need to hold the file any longer
            unset($this->photoFile);
        }
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        //
        if ($filePath = $this->getAbsolutePath()) {
            unlink($filePath);
        }
    }

    /**
     * @return string $path
     */
    public function getAbsolutePath()
    {
        return null === $this->photo ? null : $this->getUploadRootDir() . '/' . $this->photo;
    }

    public function getWebPath()
    {
        return null === $this->photo ? null : $this->getUploadDir() . '/' .$this->photo;
    }

    /**
     *
     */
    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../../../web/' . $this->getUploadDir();
    }

    /**
     *
     */
    protected function getUploadDir()
    {
        return '/uploads/users';
    }
}
