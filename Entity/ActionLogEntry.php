<?php

namespace DL\AdminBundle\Entity;

use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Table(name="admin_action_log_entry")
 * @ORM\Entity()
 */
class ActionLogEntry
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $user;

    /**
     * @ORM\Column(name="username", type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(name="component", type="string", length=255)
     */
    private $component;

    /**
     * @ORM\Column(name="data", type="text")
     */
    private $data;

    /**
     * @ORM\Column(name="ipaddress", type="string", length=255)
     */
    private $ipAddress;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     *
     */
    public function __construct($user, $component, $data, $ipAddress)
    {
        $this->setUser($user);

        if (null !== $user) {
            $this->setUsername($user->getUsername());
        }

        $this->setComponent($component);
        $this->setData($data);
        $this->setIpAddress($ipAddress);
        $this->setCreatedAt(new \DateTime());
    }

    /**
     *
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the user
     *
     * @param User $user
     *
     * @return ActionLogEntry
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @Return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set the username
     *
     * @param string $username
     *
     * @return ActionLogEntry
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @Return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set component
     *
     * @param string $component
     *
     * @return ActionLogEntry
     */
    public function setComponent($component)
    {
        $this->component = $component;

        return $this;
    }

    /**
     * Get component
     *
     * @return string
     */
    public function getComponent()
    {
        return $this->component;
    }

    /**
     * Set data
     *
     * @param string $data
     *
     * @return ActionLogEntry
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set ipaddress
     *
     * @param string $ipaddress
     *
     * @return ActionLogEntry
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    /**
     * Get data
     *
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * Set createdAt
     *
     * @param  \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

}
