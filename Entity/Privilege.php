<?php

namespace DL\AdminBundle\Entity;

use Symfony\Component\Security\Core\Role\RoleInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Table(name="admin_privilege")
 * @ORM\Entity()
 */
class Privilege implements RoleInterface, \Serializable
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=30)
     */
    private $name;

    /**
     * @ORM\Column(name="role", type="string", length=100, unique=true)
     */
    private $role;

    /**
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="privileges")
     * @ORM\JoinTable(name="admin_user_privilege")
     */
    private $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * @see RoleInterface;
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param  string    $name
     * @return Privilege
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set role
     *
     * @param  string    $role
     * @return Privilege
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Add users
     *
     * @param  DL\AdminBundle\Entity\User $users
     * @return Privilege
     */
    public function addUser(\DL\AdminBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param DL\AdminBundle\Entity\User $users
     */
    public function removeUser(\DL\AdminBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set description
     *
     * @param  string    $description
     * @return Privilege
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get the name and description
     *
     * @return string;
     */
    public function __toString()
    {
        return $this->getDescription() != '' ? $this->getName() . ' - ' . $this->getDescription() : $this->getName();
    }

    /**
     *
     */
    public function serialize()
    {
        return json_encode(array(
            $this->id,
            $this->role,
            $this->description
        ));
    }

    /**
     *
     */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->role,
            $this->description
        ) = json_decode($serialized, true);
    }
}
