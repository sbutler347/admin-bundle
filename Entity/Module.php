<?php

namespace DL\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Module
 *
 * @ORM\Entity
 * @ORM\Table(name="module")
 */
class Module
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="bundle_name", type="string", length=255)
     */
    private $bundleName;

    /**
     * @var string
     *
     * @ORM\Column(name="bundle_class_path", type="string", length=510)
     */
    private $bundleClassPath;

    /**
     * @var string
     *
     * @ORM\Column(name="entity_name", type="string", length=255)
     */
    private $entityName;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_controller_name", type="string", length=255)
     */
    private $adminControllerName;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_controller_route", type="string", length=255, nullable=true)
     */
    private $adminControllerRoute;

    /**
     * @var string
     *
     * @ORM\Column(name="required_role", type="string", length=255);
     */
    private $requiredRole;

    /**
     * @var integer
     *
     * @ORM\Column(name="menu_level", type="integer", nullable=true)
     */
    private $menuLevel;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param  string    $name
     * @return Component
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param  string    $description
     * @return Component
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set adminControllerRoute
     *
     * @param  string    $adminControllerRoute
     * @return Component
     */
    public function setAdminControllerRoute($adminControllerRoute)
    {
        $this->adminControllerRoute = $adminControllerRoute;

        return $this;
    }

    /**
     * Get adminControllerRoute
     *
     * @return string
     */
    public function getAdminControllerRoute()
    {
        return $this->adminControllerRoute;
    }

    /**
     * Set requiredRole
     *
     * @param  string $requiredRole
     * @return Module
     */
    public function setRequiredRole($requiredRole)
    {
        $this->requiredRole = $requiredRole;

        return $this;
    }

    /**
     * Get requiredRole
     *
     * @return string
     */
    public function getRequiredRole()
    {
        return $this->requiredRole;
    }

    /**
     * Set bundleName
     *
     * @param  string $bundleName
     * @return Module
     */
    public function setBundleName($bundleName)
    {
        $this->bundleName = $bundleName;

        return $this;
    }

    /**
     * Get bundleName
     *
     * @return string
     */
    public function getBundleName()
    {
        return $this->bundleName;
    }

    /**
     * Set bundleClassPath
     *
     * @param  string $bundleClassPath
     * @return Module
     */
    public function setBundleClassPath($bundleClassPath)
    {
        $this->bundleClassPath = $bundleClassPath;

        return $this;
    }

    /**
     * Get bundleClassPath
     *
     * @return string
     */
    public function getBundleClassPath()
    {
        return $this->bundleClassPath;
    }

    /**
     * Set name
     *
     * @param  string    $entityName
     * @return Component
     */
    public function setEntityName($entityName)
    {
        $this->entityName = $entityName;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getEntityName()
    {
        return $this->entityName;
    }

    /**
     * Set adminControllerName
     *
     * @param  string $adminControllerName
     * @return Module
     */
    public function setAdminControllerName($adminControllerName)
    {
        $this->adminControllerName = $adminControllerName;

        return $this;
    }

    /**
     * Get adminControllerName
     *
     * @return string
     */
    public function getAdminControllerName()
    {
        return $this->adminControllerName;
    }

    /**
     * Set menuLevel
     *
     * @param  string    $menuLevel
     * @return Component
     */
    public function setMenuLevel($menuLevel)
    {
        $this->menuLevel = $menuLevel;

        return $this;
    }

    /**
     * Get menuLevel
     *
     * @return string
     */
    public function getMenuLevel()
    {
        return $this->menuLevel;
    }

    /**
     * Set createdAt
     *
     * @param  \DateTime $createdAt
     * @return Component
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
