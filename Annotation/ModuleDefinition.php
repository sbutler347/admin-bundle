<?php

namespace DL\AdminBundle\Annotation;

/**
 * @Annotation
 */
class ModuleDefinition
{
    private $name;
    private $description;
    private $bundleName;
    private $bundleClassPath;
    private $entityName;
    private $adminControllerName;
    private $adminControllerRoute;
    private $requiredRole;
    private $menuLevel = null;

    public function __construct($options)
    {
        foreach ($options as $key => $value) {
            if (!property_exists($this, $key)) {
                throw new \InvalidArgumentException(sprintf('Module definition does not contain property "%s". %s', $key, print_r($options, true)));
            }

            $this->$key = $value;
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getBundleName()
    {
        return $this->bundleName;
    }

    /**
     * @return string
     */
    public function getBundleClassPath()
    {
        return $this->bundleClassPath;
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return $this->entityName;
    }

    /**
     * @return string
     */
    public function getAdminControllerName()
    {
        return $this->adminControllerName;
    }

    /**
     * @return string
     */
    public function getAdminControllerRoute()
    {
        return $this->adminControllerRoute;
    }

    /**
     * @return string
     */
    public function getRequiredRole()
    {
        return $this->requiredRole;
    }

    /**
     * @return integer
     */
    public function getMenuLevel()
    {
        return $this->menuLevel;
    }

    /**
     * This method is required/used for the array_unique by the annotation_finder service
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getBundleName() . ':' . $this->getAdminControllerName();
    }
}
