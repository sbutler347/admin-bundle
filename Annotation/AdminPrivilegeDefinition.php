<?php

namespace DL\AdminBundle\Annotation;

/**
 * @Annotation
 */
class AdminPrivilegeDefinition
{
    private $role;
    private $name;
    private $description;

    public function __construct($options)
    {
        foreach ($options as $key => $value) {
            if (!property_exists($this, $key)) {
                throw new \InvalidArgumentException(sprintf('Property "%s" does not exist', $key));
            }

            $this->$key = $value;
        }
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * This method is required/used for the array_unique by the annotation_finder service
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getRole();
    }
}
