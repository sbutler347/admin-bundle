admin-bundle
===============

DotLabel Admin Bundle (Containing UI/Admin Account Management) 

## Todo
[X] Change the privlege command to use the new annotation service
[X] Add a Module annotation @DL\Module(name="ModuleName", description="ModuleDescription", isPageModule=true, routeName="admin_news") 
[X] Add a PrivilegeDefinition module @DL\PrivilegeDefinition(name="", description="", role="ROLE_THIS")
[ ] Add page creation with tab module assigment. 

## Prerequisites

This version of the bundle requires Symfony 2.3.

## Installation

1. Download DLAdminBundle using composer
2. Enable the Bundle
3. Update your database schema

### Step 1: Download DLAdminBundle using composer

Add DLAdminBundle in your composer.json:

```js
{
    "require": {
        "dotlabel/admin-bundle": "*"
    }
}
```

Now tell composer to download the bundle by running the command:

``` bash
$ php composer.phar update dotlabel/admin-bundle
```

Composer will install the bundle to your project's `vendor/dotlabel` directory.

### Step 2: Enable the bundle

Enable the bundle in the kernel:

``` php
<?php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new DL\AdminBundle\DLAdminBundle(),
    );
}
```

### Step 3: Update your database schema

Now that the bundle is configured, the last thing you need to do is update your
database schema because you have added a new entity, the `User` class which you
created in Step 4.

For ORM run the following command.

``` bash
$ php app/console doctrine:schema:update --force
```

### Step 4: Configure the security 

The app/config/routing.yml and app/config/security.yml files still need updated

#### Sample routing.yml 

``` yaml
# app/config/routing.yml 

admin_login: 
    pattern: /%admin_root%/auth/login
    defaults: { _controller: DLAdminBundle:Security:login }

admin_login_check:
    pattern: /%admin_root%/auth/login_check
    requirements: { _method: POST }

admin_logout:
    pattern: /%admin_root%/auth/logout

DLAdminBundlePasswordReset:
    resource: "@DLAdminBundle/Controller/PasswordResetController.php"
    type: annotation
    prefix: /%admin_root%/auth/reset
```

#### Sample security.yml

``` yaml
# app/config/security.yml
security:
    encoders:
        DL\AdminBundle\Entity\User: sha512

    role_hierarchy:
        ROLE_ADMIN:       ROLE_USER
        ROLE_SUPER_ADMIN: [ROLE_USER, ROLE_USER_ADMIN, ROLE_ADMIN, ROLE_ALLOWED_TO_SWITCH]


    firewalls:
        admin_area:
            pattern: ^/%admin_root%
            anonymous: ~
            form_login:
                login_path: /%admin_root%/auth/login
                check_path: /%admin_root%/auth/login_check
                default_target_path: /%admin_root%
                always_use_default_target_path: true
                success_handler: authentication_success_handler
            logout:
                path: /%admin_root%/auth/logout
                target: /
        frontend:
            pattern: ^/
            anonymous: ~
       
        dev:
            pattern:  ^/(_(profiler|wdt)|css|images|js)/
            security: false

    access_control:
        - { path: ^/%admin_root%/auth/reset, roles: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/%admin_root%/auth/login, roles: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/%admin_root%, roles: ROLE_ADMIN }

    providers:
        admin_user_db: 
            entity: { class: DLAdminBundle:User }
```
