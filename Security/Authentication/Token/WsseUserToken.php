<?php

namespace DL\AdminBundle\Security\Authentication\Token;

use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

class WsseUserToken extends AbstractToken
{
    private $created;
    private $digest;
    private $nonce;

    public function __construct(array $roles=array())
    {
        parent::__construct($roles);

        $this->setAuthenticated(count($roles) > 0);
    }

    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function setDigest($digest)
    {
        $this->digest = $digest;

        return $this;
    }

    public function getDigest()
    {
        return $this->digest;
    }

    public function setNonce($nonce)
    {
        $this->nonce = $nonce;

        return $this;
    }

    public function getNonce()
    {
        return $this->nonce;
    }

    public function getCredentials()
    {
        return '';
    }
}
