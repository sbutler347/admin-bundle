<?php
namespace DL\AdminBundle\Security\Authentication\Provider;

use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\NonceExpiredException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use DL\AdminBundle\Security\Authentication\Token\WsseUserToken;

class WsseProvider implements AuthenticationProviderInterface
{
    private $userProvider;
    private $factory;
    private $cacheDir;
    private $encoder;

    public function __construct(UserProviderInterface $userProvider, $factory, $cacheDir, $logger)
    {
        $this->userProvider = $userProvider;
        $this->factory      = $factory;
        $this->cacheDir     = $cacheDir;
        $this->logger       = $logger;
    }

    public function authenticate(TokenInterface $token)
    {
        $user = $this->userProvider->loadUserByUsername($token->getUsername());

        $this->logger->err("CHRIS We got a user woot");

        // get the correct encoder for this user
        $this->encoder            = $this->factory->getEncoder($user);

        if ($user && $this->validateDigest($token->getDigest(), $token->getNonce(), $token->getCreated(), $user->getPassword(), $user->getSalt())) {
            $authenticatedToken = new WsseUserToken($user->getRoles());
            $authenticatedToken->setUser($user);

            return $authenticatedToken;
        }

        throw new AuthenticationException('The WSSE authentication failed.');
    }

    /**
     * This function is specific to Wsse authentication and is only used to help this example
     *
     * For more information specific to the logic here, see
     * https://github.com/symfony/symfony-docs/pull/3134#issuecomment-27699129
     */
    protected function validateDigest($digest, $nonce, $created, $secret, $salt)
    {
        $this->logger->err(sprintf("CHRIS: Validating digest with %s %s %s %s %s", $digest, $nonce, $created, $secret, $salt));

        // Check created time is not in the future
        if (strtotime($created) > time()) {
            $this->logger->err("CHRIS: created time is in the future");
            return false;
        }

        // Expire timestamp after 5 minutes
        if (time() - strtotime($created) > 300) {
            $this->logger->err("CHRIS: this is more than 5 minutes ago");
            return false;
        }

        // Validate that the nonce is *not* used in the last 5 minutes
        // if it has, this could be a replay attack
        if (file_exists($this->cacheDir . '/' . $nonce) && file_get_contents($this->cacheDir . '/' . $nonce) + 300 > time()) {
            $this->logger->err("CHRIS: previously used nonce");
            throw new NonceExpiredException('Previously used nonce detected');
        }

        // If cache directory does not exist we create it
        if (!is_dir($this->cacheDir)) {
            mkdir($this->cacheDir, 0777, true);
        }

        file_put_contents($this->cacheDir . '/' . $nonce, time());

        // Validate Secret
        // $expected = base64_encode(sha1(base64_decode($nonce) . $created . $secret, true));
        $this->logger->err(sprintf("CHRIS: created nonce folder, going to test password"));
       
        $expected = base64_encode(sha1(base64_decode($nonce).$created.$secret, true));
        // $encPassword = $this->encoder->encodePassword($

        /**
        $expected = $this->encoder->encodePassword(
            sprintf(
                '%s%s%s',
                base64_decode($nonce),
                $created,
                $secret
            ),
            $salt
        );
        **/
        
        $this->logger->err(sprintf("The expected (%s) is (%s)", $expected, $digest)); 

        return $digest === $expected;
    }

    public function supports(TokenInterface $token)
    {
        return $token instanceof WsseUserToken;
    }
}
